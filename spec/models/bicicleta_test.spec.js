var Bicicleta = require('../../models/bicicleta');

// Precondicuon que se ejecuta antes de cada test
beforeEach(() => {Bicicleta.allBicis = []; });

// describe es un grupo de test
describe('Bicicleta.allBicis', () => {
    it('comienza vacia', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
    });    
});

describe('Bicicleta.add', () => {
    it('agregamos una', () => {
        // Precondición
        expect(Bicicleta.allBicis.length).toBe(0);
        var a = new Bicicleta(1,'rojo','urbana',[-34.6012424,-58.3861497]);
        Bicicleta.add(a);
        // Condición final
        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);

    });    
});

describe('Bicicleta.findById', () => {
    it('debe devolver la bici con id 1', () => {
        // Precondición
        expect(Bicicleta.allBicis.length).toBe(0);
        var aBici1 = new Bicicleta(1,'verde','urbana',[-34.6012424,-58.3861497]);
        var aBici2 = new Bicicleta(2,'roja','montaña',[-34.6012424,-58.3861497]);
        Bicicleta.add(aBici1);
        Bicicleta.add(aBici2);
        // Condición final
        var targetBici = Bicicleta.findById(1);
        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(aBici1.color);
        expect(targetBici.modelo).toBe(aBici1.modelo);
    });    
});

describe('Bicicleta.removeById', () => {
    it('debe borrar la bici 1', () => {
        // Estado previo desde 0
        expect(Bicicleta.allBicis.length).toBe(0);
        // Creamos la bicicleta
        var aBici = new Bicicleta(1, 'verde', 'urbana');
        Bicicleta.add(aBici);
        // Usamos el metodo remove
        var targetBici = Bicicleta.findById(1);
        expect(targetBici.id).toBe(1);
        process.stdout.write("Quitamos la bicicleta id: ");
        process.stdout.write(targetBici.id.toString());
        var removeBici = Bicicleta.removeById(1);
        expect(Bicicleta.allBicis.length).toBe(0);
    });
});