var map = L.map('main_map').setView([19.3274789,-99.1823378],17);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',{
    attribution:'&copy;<a hfref = "https://www.openstreetmap.org/copyrigth">OpenStreetmap</a> contributors'
}).addTo(map);
L.marker([19.3274789,-99.1823378],{title: "iOS Lab"}).addTo(map);

$.ajax({
    datatype:"json",
    url:"api/bicicletas",
    success:function(result){
        console.log(result);
        result.bicicletas.forEach(function(bici){
            L.marker(bici.ubicacion,{title: bici.id}).addTo(map);
        });
    }
})