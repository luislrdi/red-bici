# Proyecto en NodeJS usando EXPRESS => red-bici

## Creo la aplicación  con Express
`express --view=png red-bici` Especificar la extensión con png es necesaria, sino coloca los archivos error.png, index.png y layout.png con .jade en la carpeta view.

## Instalo la dependencias en package.json
`npm install`

## Agrego .gitignore
node_modules

## Creo el repositorio red-bici en Bitbucked y lo conecto con el local
`git remote add origin git@bitbucked.org:luislrdi/red-bici.git`

`git push -u origin master`

## Descargo el template grayscale
`https://startbootstrap.com/theme/grayscale` en la carpeta local red-bici

## Arranco el servidor
`npm start`

## Para detectar modificaciones instalo nodemon
`npm install nodedemon --save-dev` para ejecutarlo en modo desarrollo.

## Agrego devstart en package.json para ejecutar nodemon
`npm run devstart`

## Configuro mapa de leafletjs.com
Usamos como proveedor de mapas a openstreetmap.

## Importante la libreria https://html-to-pug.com/
Para convertir la sintaxis de html a pug.
